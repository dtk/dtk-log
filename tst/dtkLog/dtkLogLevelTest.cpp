// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogLevelTest.h"

#include <dtkLogTest>

#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
// dtkLogLevelTestCasePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkLogLevelTestCasePrivate
{
public:
    QTemporaryFile file;
    QTextStream *in = nullptr;
};

// ///////////////////////////////////////////////////////////////////

dtkLogLevelTestCase::dtkLogLevelTestCase(void) : d(new dtkLogLevelTestCasePrivate)
{
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
    d->file.open();
    dtkLogger::instance().attachFile(d->file.fileName());

    d->in = new QTextStream(&d->file);
}

dtkLogLevelTestCase::~dtkLogLevelTestCase(void)
{
    delete d->in;
    delete d;
}

void dtkLogLevelTestCase::initTestCase(void)
{

}

void dtkLogLevelTestCase::init(void)
{
}

void dtkLogLevelTestCase::testTrace(void)
{
    dtkLogger::instance().attachConsole();

    dtkTrace() << "Trace log";
    QString line = d->in->readLine();
    QVERIFY(line.contains("[TRACE"));
    QVERIFY(line.contains("Trace log"));

    dtkLog(dtk::LogLevel::Trace) << "Custom Trace log";
    line = d->in->readLine();
    QVERIFY(line.contains("[TRACE"));
    QVERIFY(line.contains("Custom Trace log"));
}

void dtkLogLevelTestCase::testDebug(void)
{
    dtkDebug() << "Debug" << "log";
    QString line = d->in->readLine();
    QVERIFY(line.contains("[DEBUG"));
    QVERIFY(line.contains("Debug log"));

    dtkLog(dtk::LogLevel::Debug) << "Custom Debug log";
    line = d->in->readLine();
    QVERIFY(line.contains("[DEBUG"));
    QVERIFY(line.contains("Custom Debug log"));
}

void dtkLogLevelTestCase::testInfo(void)
{
    dtkInfo() << "Info log";
    QString line = d->in->readLine();
    QVERIFY(line.contains("[INFO"));
    QVERIFY(line.contains("Info log"));

    dtkLog(dtk::LogLevel::Info) << "Custom Info log";
    line = d->in->readLine();
    QVERIFY(line.contains("[INFO"));
    QVERIFY(line.contains("Custom Info log"));
}

void dtkLogLevelTestCase::testWarn(void)
{
    dtkWarn() << "Warn log";
    QString line = d->in->readLine();
    QVERIFY(line.contains("[WARN"));
    QVERIFY(line.contains("Warn log"));

    dtkLog(dtk::LogLevel::Warn) << "Custom Warn log";
    line = d->in->readLine();
    QVERIFY(line.contains("[WARN"));
    QVERIFY(line.contains("Custom Warn log"));
}

void dtkLogLevelTestCase::testError(void)
{
    dtkError() << "Error log";
    QString line = d->in->readLine();
    QVERIFY(line.contains("[ERROR"));
    QVERIFY(line.contains("Error log"));

    dtkLog(dtk::LogLevel::Error) << "Custom Error log";
    line = d->in->readLine();
    QVERIFY(line.contains("[ERROR"));
    QVERIFY(line.contains("Custom Error log"));
}

void dtkLogLevelTestCase::testFatal(void)
{
    try {
        dtkFatal() << "Fatal log";
    } catch (const std::runtime_error(e)) {
        QString line = d->in->readLine();
        QVERIFY(line.contains("[FATAL"));
        QVERIFY(line.contains("Fatal log"));
    }

    try {
        dtkLog(dtk::LogLevel::Fatal) << "Custom Fatal log";
    } catch (const std::runtime_error(e)) {
        QString line = d->in->readLine();
        QVERIFY(line.contains("[FATAL"));
        QVERIFY(line.contains("Custom Fatal log"));
    }
}

void dtkLogLevelTestCase::cleanupTestCase(void)
{

}

void dtkLogLevelTestCase::cleanup(void)
{
}

DTKLOGTEST_MAIN_NOGUI(dtkLogLevelTest, dtkLogLevelTestCase);

//
// dtkLogLevelTest.cpp ends here
