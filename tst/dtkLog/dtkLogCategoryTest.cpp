// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogCategoryTest.h"

#include <dtkLogTest>

#include <dtkLog>


DTK_CATEGORY_DEFINE(myCategory, "my.category");
// ///////////////////////////////////////////////////////////////////
// dtkLogCategoryTestCasePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkLogCategoryTestCasePrivate
{
public:
    QTemporaryFile file;
    QTextStream *in = nullptr;
};

// ///////////////////////////////////////////////////////////////////

dtkLogCategoryTestCase::dtkLogCategoryTestCase(void) : d(new dtkLogCategoryTestCasePrivate)
{
    dtkLogger::instance().setLevel(dtk::LogLevel::Trace);
    d->file.open();
    dtkLogger::instance().attachFile(d->file.fileName());

    d->in = new QTextStream(&d->file);
}

dtkLogCategoryTestCase::~dtkLogCategoryTestCase(void)
{
    delete d->in;
    delete d;
}

void dtkLogCategoryTestCase::initTestCase(void)
{

}

void dtkLogCategoryTestCase::init(void)
{
}

void dtkLogCategoryTestCase::testCategory(void)
{
    dtkLogger::instance().attachConsole(true);

    dtkLog(dtkAll())           << "ALL";
    dtkLog(dtkCore())          << "CORE";
    dtkLog(dtkWidgets())       << "WIDGETS";
    dtkLog(dtkVisualization()) << "VISUALIZATION";
    dtkLog(myCategory())       << "MY CATEGORY";

    dtk::LogLevel allLevels[] =    {dtk::Trace, dtk::Debug, dtk::Info , dtk::Warn , dtk::Error, dtk::Fatal};
    for (dtk::LogLevel level: allLevels) {
        dtkLog(myCategory(), level) << QString("MY CATEGORY, %1 %2").arg( dtk::logLevelToString(level) ).arg(level);
    }

    QTextStream in(&d->file);
    QString line = in.readLine();
    QVERIFY(line.contains("[dtk.all]"));
    QVERIFY(line.contains("ALL"));
    QVERIFY(line.contains("INFO"));

    line = in.readLine();
    QVERIFY(line.contains("[dtk.core]"));
    QVERIFY(line.contains("CORE"));
    QVERIFY(line.contains("INFO"));

    line = in.readLine();
    QVERIFY(line.contains("[dtk.widgets]"));
    QVERIFY(line.contains("WIDGETS"));
    QVERIFY(line.contains("INFO"));

    line = in.readLine();
    QVERIFY(line.contains("[dtk.visualization]"));
    QVERIFY(line.contains("VISUALIZATION"));
    QVERIFY(line.contains("INFO"));

    line = in.readLine();
    QVERIFY(line.contains("[my.category"));
    QVERIFY(line.contains("MY CATEGORY"));
    QVERIFY(line.contains("INFO"));

//    for (dtk::LogLevel level =dtk::LogLevel::Trace; level < dtk::Fatal + 1; level=dtk::LogLevel( level + 1 )) {
    for (dtk::LogLevel level: allLevels) {
        line = in.readLine();
        QVERIFY(line.contains("[my.category]"));
        QVERIFY(line.contains( QString("MY CATEGORY, %1").arg( dtk::logLevelToString( level ) ) ) );
        QVERIFY(line.contains( QString("[%1]").arg( dtk::logLevelToString( level ) ) ) );
    }
}

void dtkLogCategoryTestCase::cleanupTestCase(void)
{

}

void dtkLogCategoryTestCase::cleanup(void)
{
}

DTKLOGTEST_MAIN_NOGUI(dtkLogCategoryTest, dtkLogCategoryTestCase);

//
// dtkLogCategoryTest.cpp ends here
