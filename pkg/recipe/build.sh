#!/bin/bash

if [[ -d build ]]; then
    rm -rf build
fi
mkdir build
cd build

if [[ "$c_compiler" == "gcc" ]]; then
  echo "CHANGE PATH for gcc"
  export PATH="${PATH}:${BUILD_PREFIX}/${HOST}/sysroot/usr/lib:${BUILD_PREFIX}/${HOST}/sysroot/usr/include"
fi

ARCH=`arch`
if [ $ARCH = "arm64" ] || [ $ARCH = "aarch64" ]; then
  export QT_HOST_PATH=$PREFIX
fi

cmake .. \
        -G 'Ninja' \
        -DCMAKE_PREFIX_PATH=$PREFIX \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DCMAKE_INSTALL_LIBDIR=lib

cmake --build . --target install
