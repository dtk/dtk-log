//
// Created by Erwan Demairy on 16/12/202051.
//

#include <dtkLogFormatter.h>

#include <dtkLogLevel.h>

namespace dtk {

    QString logFormatterDefault(const QHash <QString, QString> &message)
    {
        if (message.contains("category")) {
            return QString("[%1][%2] - %3 - %4")
                .arg(message["category"])
                .arg(message["level"])
                .arg(QDateTime::currentDateTime().toString())
                .arg(message["buffer"]);
        } else if (message.contains("level")) {
            return QString("[%1] - %2 - %3")
                .arg(message["level"])
                .arg(QDateTime::currentDateTime().toString())
                .arg(message["buffer"]);
        } else {
            return QString("[] - %1 - %2")
                .arg(QDateTime::currentDateTime().toString())
                .arg(message["buffer"]);
        }
    };

    QString logFormatterColor(const QHash <QString, QString> &message)
    {
        QString result;
        if (message.contains("category")) {
            result = QString("[%1][%2] - %3 - %4")
                .arg(message["category"])
                .arg(message["level"])
                .arg(QDateTime::currentDateTime().toString())
                .arg(message["buffer"]);
        } else if (message.contains("level")) {
            result = QString("[%1] - %2 - %3")
                .arg(message["level"])
                .arg(QDateTime::currentDateTime().toString())
                .arg(message["buffer"]);
        } else {
            result = QString("[] - %1 - %2")
                .arg(QDateTime::currentDateTime().toString())
                .arg(message["buffer"]);
        }
        if (message.contains("level")) {
            switch(dtk::logLevelFromString(message["level"])) {
                case dtk::LogLevel::Trace :
                    result.prepend("\033[90;40m");
                    result.append("\033[0m\n");
                    break;
                case dtk::LogLevel::Debug :
                    result.prepend("\033[30;42m");
                    result.append("\033[0m\n");
                    break;
                case dtk::LogLevel::Info :
                    result.prepend("\033[30;106m");
                    result.append("\033[0m\n");
                    break;
                case dtk::LogLevel::Warn :
                    result.prepend("\033[30;103m");
                    result.append("\033[0m\n");
                    break;
                case dtk::LogLevel::Error :
                    result.prepend("\033[30;101m");
                    result.append("\033[0m\n");
                    break;
                case dtk::LogLevel::Fatal :
                    result.prepend("\033[5;30;101m");
                    result.append("\033[0m\n");
                    break;
            }
        }
        return result;
    }

}
