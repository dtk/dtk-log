// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogExport>

#include "dtkLogLevel.h"

#include <QtCore>

class DTKLOG_EXPORT dtkLogEngine
{
public:
     dtkLogEngine(dtk::LogLevel level, bool custom = false);
     dtkLogEngine(dtk::LogLevel level, const QLoggingCategory& category);
    ~dtkLogEngine(void) noexcept(false);

    class QDebug stream(void) const;

private:
    class dtkLogEnginePrivate *d;
};

//
// dtkLogEngine.h ends here
