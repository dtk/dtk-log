// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogCategory.h"

Q_LOGGING_CATEGORY(dtkAll, "dtk.all");
Q_LOGGING_CATEGORY(dtkCore, "dtk.core");
Q_LOGGING_CATEGORY(dtkWidgets, "dtk.widgets");
Q_LOGGING_CATEGORY(dtkVisualization, "dtk.visualization");

namespace dtk {

    static QLoggingCategory *default_log_category = &(const_cast<QLoggingCategory&>(dtkAll()));

    void setDefaultLogCategory(const QLoggingCategory& log_category)
    {
        default_log_category = &(const_cast<QLoggingCategory&>(log_category));
    }

    const QLoggingCategory& defaultLogCategory(void)
    {
        return *default_log_category;
    }
}

//
// dtkLogCategory.cpp ends here
