// Version: $Id: 82dae51109d79bd8ab088c24452fe24fcec09fc8 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogger.h"
#include "dtkLogger_p.h"
#include "dtkLog.h"

#include <iostream>

#include <QtWebSockets>

// ///////////////////////////////////////////////////////////////////
// Helper class implementation
// ///////////////////////////////////////////////////////////////////

namespace dtk {
    redirectStream::int_type redirectStream::overflow(int_type v)
    {
        if (v == '\n') {
            dtkLog(m_level) << "";
        }
        return v;
    }
    std::streamsize redirectStream::xsputn(const char *p, std::streamsize n)
    {
        QString str(p);
        dtkLog(m_level) << str;
        return n;
    }
}

// ///////////////////////////////////////////////////////////////////
// dtkLogger implementation
// ///////////////////////////////////////////////////////////////////

dtkLogger::dtkLogger(void) : d(new dtkLoggerPrivate)
{
    d->level = dtk::LogLevel::Info;
    d->cerr_stream = nullptr;
    d->cout_stream = nullptr;
    d->console = std::make_shared<dtkLogDestinationConsole>();
    d->color_console = std::make_shared<dtkLogDestinationConsole>(true);
}

dtkLogger::~dtkLogger(void)
{
    if (d->cerr_stream) {
        delete d->cerr_stream;
    }
    if (d->cout_stream) {
        delete d->cout_stream;
    }
    delete d;
}

dtkLogger& dtkLogger::instance(void)
{
    static dtkLogger log;
    return log;
}

dtk::LogLevel dtkLogger::level(void) const
{
    return d->level;
}

QString dtkLogger::levelString(void) const
{
    return dtk::logLevelToString(d->level);
}

void dtkLogger::setLevel(dtk::LogLevel level)
{
    d->level = level;
}

void dtkLogger::setLevel(const QString& level_name)
{
    d->level = dtk::logLevelFromString(level_name);
}

void dtkLogger::attachConsole(bool color)
{
    if (color) {
        if (!d->destinations.contains(d->color_console)) {
            d->destinations << d->color_console;
        }
    } else {
        if (!d->destinations.contains(d->console)) {
            d->destinations << d->console;
        }
    }
}

void dtkLogger::attachConsole(dtk::LogLevel level, bool color)
{
    attachConsole(color);
    if (!color) {
        d->levels[d->console.get()] = level;
    } else {
        d->levels[d->color_console.get()] = level;
    }
}

void dtkLogger::detachConsole(void)
{
    d->destinations.removeOne(d->console);
    d->destinations.removeOne(d->color_console);
}

void dtkLogger::attachWebSocket(QWebSocket *socket)
{
    if (d->webSockets.contains(socket)) {
        return;
    }

    d->webSockets[socket] = std::make_shared<dtkLogDestinationWebSocket>(socket);

    d->destinations << d->webSockets[socket];
}

void dtkLogger::attachWebSocket(QWebSocket *socket, dtk::LogLevel level)
{
    attachWebSocket( socket );
    d->levels[d->webSockets[socket].get()] = level;
}

void dtkLogger::detachWebSocket(QWebSocket *socket)
{
    if (!d->webSockets.contains(socket)) {
        return;
    }
    socket->disconnect();

    d->destinations.removeOne(d->webSockets[socket]);

    d->webSockets.remove(socket);
}

void dtkLogger::attachFile(const QString& path, qlonglong max_file_size)
{
    if (d->files.contains(path)) {
        return;
    }

    d->files[path] = std::make_shared<dtkLogDestinationFile>(path, max_file_size);

    d->destinations << d->files[path];
}

void dtkLogger::attachFile(const QString& path, dtk::LogLevel level, qlonglong max_file_size)
{
    attachFile( path, level, max_file_size );
    d->levels[ d->files[path].get() ] = level;
}


void dtkLogger::detachFile(const QString& path)
{
    if (!d->files.contains(path)) {
        return;
    }

    d->destinations.removeOne(d->files[path]);

    d->files.remove(path);
}

void dtkLogger::attachModel(dtkLogModel *model)
{
    if (d->models.contains(model))
        return;

    d->models[model] = dtkLogDestinationPointer(new dtkLogDestinationModel(model));

    d->destinations << d->models[model];
}

void dtkLogger::detachModel(dtkLogModel *model)
{
    if (!d->models.contains(model)) {
        return;
    }

    d->destinations.removeOne(d->models[model]);

    d->models.remove(model);
}

QMetaObject::Connection dtkLogger::attachSlot(const QObject *receiver, const char *method, Qt::ConnectionType type)
{
    auto key = qMakePair(receiver, method);

    if (d->m_slots.contains(key)){
        return d->m_slots[key]->connection();
    }
        
    d->m_slots[key] = std::shared_ptr<dtkLogDestinationSlot>(new dtkLogDestinationSlot(receiver, method, type));

    d->destinations << d->m_slots[key];

    return d->m_slots[key]->connection();
}

void dtkLogger::detachSlot(const QObject *receiver, const char *method){

    auto key = qMakePair(receiver, method);

    if (!d->m_slots.contains(key)) {
        return;
    }

    d->destinations.removeOne(d->m_slots[key]);

    d->m_slots.remove(key);
}

void dtkLogger::write(const QHash<QString, QString>& message)
{
    for (int i = 0; i < d->destinations.count(); ++i) {
        d->destinations.at(i)->write(message);
    }
}

void dtkLogger::write(const QHash<QString, QString>& message, dtk::LogLevel level)
{
    for (int i = 0; i < d->destinations.count(); ++i) {
        if (d->levels.keys().contains(d->destinations.at(i).get())) {
            if (level > d->levels[d->destinations.at(i).get()]) {
                d->destinations.at(i)->write(message);
            }
        } else {
            d->destinations.at(i)->write(message);
        }
    }
}

void dtkLogger::redirectCout(dtk::LogLevel level)
{
    d->cout_stream =  new dtk::redirectStream(std::cout, level);
}

void dtkLogger::redirectCerr(dtk::LogLevel level)
{
    d->cerr_stream =  new dtk::redirectStream(std::cerr, level);
}

//
// dtkLogger.cpp ends here
