### CMakeLists.txt ---
##

project(dtkLog
VERSION
  ${dtkLog_VERSION}
LANGUAGES
  CXX)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS
  dtkLog
  dtkLog.h
  dtkLogCategory
  dtkLogCategory.h
  dtkLogDestination
  dtkLogDestination.h
  dtkLogEngine
  dtkLogEngine.h
  dtkLogger
  dtkLogger.h
  dtkLogger_p.h
  dtkLogLevel
  dtkLogLevel.h
  dtkLogModel
  dtkLogModel.h
  dtkLogFormatter
  dtkLogFormatter.h
)

set(${PROJECT_NAME}_SOURCES
  dtkLogCategory.cpp
  dtkLogDestination.cpp
  dtkLogEngine.cpp
  dtkLogger.cpp
  dtkLogLevel.cpp
  dtkLogModel.cpp
  dtkLogFormatter.cpp
)

## ###################################################################
## Config file
## ###################################################################

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.h.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h")
configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.h.in"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.h"
  "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config")

## #################################################################
## Build rules
## #################################################################

qt_add_library(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

target_include_directories(${PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>/..
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>/..
  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
  $<INSTALL_INTERFACE:include>)

target_link_libraries(${PROJECT_NAME} PRIVATE Qt6::WebSockets)
target_link_libraries(${PROJECT_NAME} PUBLIC Qt6::Core)

add_library(dtk::Log ALIAS dtkLog)

## #################################################################
## Target properties
## #################################################################

set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${${PROJECT_NAME}_VERSION}
                                                 SOVERSION ${${PROJECT_NAME}_VERSION_MAJOR})

## #################################################################
## Export header file
## #################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export"
 "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(
FILES
  ${${PROJECT_NAME}_HEADERS}
DESTINATION
  include/${PROJECT_NAME})

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

install(EXPORT ${PROJECT_NAME}-targets
FILE
  ${PROJECT_NAME}Targets.cmake
DESTINATION
  ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME})

export(EXPORT ${PROJECT_NAME}-targets
FILE
  ${CMAKE_BINARY_DIR}/${PROJECT_NAME}Targets.cmake)

######################################################################
### CMakeLists.txt ends here
