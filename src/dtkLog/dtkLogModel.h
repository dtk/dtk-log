// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkLogExport>

#include <QtCore>

// ///////////////////////////////////////////////////////////////////
// dtkLogModel declaration
// ///////////////////////////////////////////////////////////////////

class DTKLOG_EXPORT dtkLogModel : public QAbstractListModel
{
    Q_OBJECT

public:
      dtkLogModel(QObject *parent = 0);
     ~dtkLogModel(void);

    void append(const QString& message);

    int rowCount(const QModelIndex& parent = QModelIndex()) const;

    QVariant data(const QModelIndex& index, int role) const;
    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

    Qt::ItemFlags flags(const QModelIndex& index) const;

    bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex());
    bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

    void sort(int column, Qt::SortOrder order = Qt::AscendingOrder);

    Qt::DropActions supportedDropActions(void) const;

private:
    class dtkLogModelPrivate *d;
};

//
// dtkLogModel.h ends here
