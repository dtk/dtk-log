//
// Created by Erwan Demairy on 16/12/202051.
//

#pragma once

#include <dtkLogExport>

#include <QtCore>

namespace dtk {

    using logFormatter = std::function<QString (const QHash<QString, QString>&)>;

    QString logFormatterDefault(const QHash<QString, QString>&);
    QString logFormatterColor(const QHash<QString, QString>&);

}
