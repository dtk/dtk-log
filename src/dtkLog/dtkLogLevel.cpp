// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkLogLevel.h"

#include <QtCore/QString>

namespace dtk {
    QString logLevelToString(LogLevel level)
    {
        switch (level) {
        case LogLevel::Trace: return QStringLiteral("TRACE");   break;
        case LogLevel::Debug: return QStringLiteral("DEBUG");   break;
        case LogLevel::Info:  return QStringLiteral("INFO ");   break;
        case LogLevel::Warn:  return QStringLiteral("WARN ");   break;
        case LogLevel::Error: return QStringLiteral("ERROR");   break;
        case LogLevel::Fatal: return QStringLiteral("FATAL");   break;
        default:              return QStringLiteral("UNKNOWN"); break;
        };
    }
    LogLevel logLevelFromString(const QString& level_name)
    {
        LogLevel level;
        if (level_name.contains("trace", Qt::CaseInsensitive)) {
            level = LogLevel::Trace;

        } else if (level_name.contains("debug", Qt::CaseInsensitive)) {
            level = LogLevel::Debug;

        } else if (level_name.contains("info", Qt::CaseInsensitive)) {
            level = LogLevel::Info;

        } else if (level_name.contains("warn", Qt::CaseInsensitive)) {
            level = LogLevel::Warn;

        } else if (level_name.contains("error", Qt::CaseInsensitive)) {
            level = LogLevel::Error;

        } else if (level_name.contains("fatal", Qt::CaseInsensitive)) {
            level = LogLevel::Fatal;

        } else {
            level = LogLevel::Info;
        }

        return level;
    }
}


//
// dtkLogLevel.cpp ends here
